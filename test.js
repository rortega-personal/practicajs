import { expect } from 'chai';
import { Afp, Isr, Sfs } from './funciones.js'

//var assert = require('assert');

describe('Función Cálculo AFP', () => {
    it('AFP should return 530.95 when 18500 is sent', () => {
        const value = Afp(18500);
        const resultado = 530.95;

        expect(value).to.be.equal(resultado);
    });

    it('AFP should return 717.50 when 25000 is sent', () => {
        const value = Afp(25000);
        const resultado = 717.50;

        expect(value).to.be.equal(resultado);
    });

    it('AFP should return 6532.12 when 250000 is sent', () => {
        const value = Afp(250000);
        const resultado = 6532.12;

        expect(value).to.be.equal(resultado);
    });

})

describe('Función Cálculo ISR', () => {
    it('El ISR de 10 mil debería retornar 0.00', () => {
        const value = Isr(10000);
        const resultado = 0.00;

        expect(value).to.be.equal(resultado);
    });
    
    it('El ISR de 40 mil debería retornar 442.65', () => {
        const value = Isr(40000);
        const resultado = 442.65;

        expect(value).to.be.equal(resultado);
    });
    
    it('El ISR de 60 mil debería retornar 3486.65', () => {
        const value = Isr(60000);
        const resultado = 3486.65;

        expect(value).to.be.equal(resultado);
    });
    
    it('El ISR de 80 mil debería retornar 7400.94', () => {
        const value = Isr(80000);
        const resultado = 7400.94;

        expect(value).to.be.equal(resultado);
    });
    
    it('El ISR de 160 mil debería retornar 26570.06', () => {
        const value = Isr(160000);
        const resultado = 26570.06;

        expect(value).to.be.equal(resultado);
    });
})

describe('Función Cálculo SFS', () => {
    it('SFS should return 3459.52 when 185700 is sent', () => {
        const value = Sfs(185700);
        const resultado = 3459.52;

        expect(value).to.be.equal(resultado);
    });

    it('SFS should return 1064 when 35000 is sent', () => {
        const value = Sfs(35000);
        const resultado = 1064;

        expect(value).to.be.equal(resultado);
    });

    it('SFS should return 273.6 when 9000 is sent', () => {
        const value = Sfs(9000);
        const resultado = 273.6;

        expect(value).to.be.equal(resultado);
    });

    it('SFS should return 2657.6 when 9000, 2 is sent', () => {
        const value = Sfs(9000, 2);
        const resultado = 2657.6;

        expect(value).to.be.equal(resultado);
    });

    it('SFS should return 2256 when 35000, 1 is sent', () => {
        const value = Sfs(35000, 1);
        const resultado = 2256;

        expect(value).to.be.equal(resultado);
    });

    it('SFS should return 7035.52 when 185700, 3 is sent', () => {
        const value = Sfs(185700, 3);
        const resultado = 7035.52;

        expect(value).to.be.equal(resultado);
    });


})