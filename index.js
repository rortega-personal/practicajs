console.log('Hello World');

function smallestCommons(arr) {
    let multiples1 = multiples(arr[0], arr[1]);
    let multiples2 = multiples(arr[1], arr[0]);
  
    for(let j=0; j<multiples1.length; j++) {
      if (multiples2.includes(multiples1[j])) {
        return multiples1[j];
      }
    }
}
  
function multiples(num1, num2) {
    let result = [];
    for(let i=1; i<=num2; i++) {
      result.push(num1*i);
    }
    return result;
}

document.write(smallestCommons([2,3]));