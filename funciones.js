
export function Afp(value) {
    const SalarioMinimo = 11380;
    return parseFloat((Math.min(value*0.0287, SalarioMinimo*20*0.0287)).toFixed(2));
}

export function Sfs(value, dependA = null) {
    const SalarioMinimo = 11380;
    let sfs = 0.0304;
    let costoDepAdc = 1192;

    if (dependA < 0 )
        return parseFloat((Math.min(value*sfs, SalarioMinimo*10*sfs)).toFixed(2));
    else
        return parseFloat((Math.min(value*sfs, SalarioMinimo*10*sfs)).toFixed(2)) + (dependA * costoDepAdc);
}

export function Ss(value) {
    return Afp(value) + Sfs(value);
}

export function Isr(value) {
    let salarioMensualGravable = value - Ss(value);
    let salarioAnual = salarioMensualGravable * 12;
    let rentaExcenta = 416220;
    let rentaA = 624329;
    let porcentajeA = 0.15;
    let rentaB = 867123;
    let porcentajeB = 0.20;
    let tasaB = 31216;
    let porcentajeC = 0.25;
    let tasaC = 79776;
    let isr = 0;
    
    if (salarioAnual <= rentaExcenta) 
            return isr;
        else if (salarioAnual <= rentaA)
                {
                isr = ((porcentajeA * (salarioAnual - rentaExcenta) / 12)).toFixed(2);
                return parseFloat(isr);
                 }
        else if (salarioAnual <= rentaB)
               { 
                isr = ((((salarioAnual - rentaA) * porcentajeB) + tasaB) /12).toFixed(2);
                return parseFloat(isr);
                }
        else 
                isr = ((((salarioAnual - rentaB) * porcentajeC) + tasaC) / 12).toFixed(2);
                return parseFloat(isr);
}

function CalcularNomina(valores) {
        
    valores.forEach(item => {
        let total;
        let afp, sfs, isr;
        if (item[2]== undefined) {item.push(0)};
        item.push(sfs = Sfs(item[1],item[2]));
        item.push(afp = Afp(item[1]));
        item.push(isr = Isr(item[1])); 
        total = parseFloat((item[1] - sfs - afp - isr).toFixed(2));
        item.push(total);
    });



    document.write("<table> <th>Empleado</th> <th>Salario</th> <th>Dependientes</th> <th>SFS</th> <th>AFP</th> <th>ISR</th> <th>TOTAL</th> ")
   valores.forEach(item => {
       document.write("<tr>");
         document.write("<td>" + item[0] +"</td>");
         document.write("<td>" + item[1] +"</td>");
         document.write("<td>" + item[2]  +"</td>");
         document.write("<td>" + item[3] +"</td>");
         document.write("<td>" + item[4] +"</td>");
         document.write("<td>" + item[5] +"</td>");
         document.write("<td>" + item[6] +"</td>");
         document.write("</tr>");
   });
    
   document.write("</table>")
   
       
}


console.table ( CalcularNomina([['Juan Perez', 185700, 3], ['Andrea Gomez', 10000], ['Lucia Nuñez', 35000, 1], ['Carolina Mejia', 26000]]) );


